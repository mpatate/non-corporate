# Non-Corporate

Everything about non-corporate (and non-government) ways to do things.

**Programming :**
- [GitLab](https://gitlab.com/)
- [Brackets](http://brackets.io/)

**Artistic :**
- [Paint.NET](https://www.getpaint.net/index.html)
- [Gimp](https://www.gimp.org/)
- [LMMS](https://lmms.io/)
- [OpenShot Video Editor](https://www.openshot.org/)
- [OBS](https://obsproject.com/)

**Computers and systems :**
- Linux

**Internet :**
- [ProtonVPN](https://protonvpn.com/)
- [ProtonMail](https://protonmail.com/)
- [Brave](https://brave.com/)
- [DuckDuckGo](https://duckduckgo.com/)
- [LBRY](https://lbry.com/)
- [WT.Social](https://wt.social/)

**Crypto Currency :**
?

**Missing :**
- A completely unbiased, user-approved news site
- A place to vote on for anything that can not be biased
- Non-corporate-owned Computers
- Non-corporate internet services

Feel free to add more to this



